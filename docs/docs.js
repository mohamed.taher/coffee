/*

this test implements 2 api's

    1- [get] http://localhost:5000/api/coffee/machines
        query strings for filtering {
            waterLineCompatible {true/false} optional
            productType {coffeeMachineSmall/coffeeMachineLarge/coffeeMachineEspresso} optional
        }
    
    2- [get] http://localhost:5000/api/coffee/pods
packSize {is number} optional
flavor {psl/mocha/hazelnut/vanilla/caramel} optional
productType {smallCoffeePod/largeCoffeePod/espressoPod}
        }        

*/