module.exports = [
    {
        name: "CM001",
        productType: "coffeeMachineSmall",
        model: "base",
        waterLineCompatible: false
    },
    {
        name: "CM002",
        productType: "coffeeMachineSmall",
        model: "premium",
        waterLineCompatible: false
    },
    {
        name: "CM003",
        productType: "coffeeMachineSmall",
        model: "deluxe",
        waterLineCompatible: true,
    },
    {
        name: "CM101",
        productType: "coffeeMachineLarge",
        model: "base",
        waterLineCompatible: false
    },
    {
        name: "CM102",
        productType: "coffeeMachineLarge",
        model: "premium",
        waterLineCompatible: true,
    },
    {
        name: "CM103",
        productType: "coffeeMachineLarge",
        model: "deluxe",
        waterLineCompatible: true,
    },
    {
        name: "EM001",
        productType: "coffeeMachineEspresso",
        model: "base",
        waterLineCompatible: false
    },
    {
        name: "EM002",
        productType: "coffeeMachineEspresso",
        model: "premium",
        waterLineCompatible: false
    },
    {
        name: "EM003",
        productType: "coffeeMachineEspresso",
        model: "deluxe",
        waterLineCompatible: true,
    },
    {
        name: "CP001",
        productType: "smallCoffeePod",
        packSize: 1,
        flavor: "vanilla"
    },
    {
        name: "CP003",
        productType: "smallCoffeePod",
        packSize: 3,
        flavor: "vanilla"
    },
    {
        name: "CP011",
        productType: "smallCoffeePod",
        packSize: 1,
        flavor: "caramel"
    },
    {
        name: "CP013",
        productType: "smallCoffeePod",
        packSize: 3,
        flavor: "caramel"
    },
    {
        name: "CP021",
        productType: "smallCoffeePod",
        packSize: 1,
        flavor: "psl"
    },
    {
        name: "CP023",
        productType: "smallCoffeePod",
        packSize: 3,
        flavor: "psl"
    },
    {
        name: "CP031",
        productType: "smallCoffeePod",
        packSize: 1,
        flavor: "mocha"
    },
    {
        name: "CP033",
        productType: "smallCoffeePod",
        packSize: 3,
        flavor: "mocha"
    },
    {
        name: "CP041",
        productType: "smallCoffeePod",
        packSize: 1,
        flavor: "hazelnut"
    },
    {
        name: "CP043",
        productType: "smallCoffeePod",
        packSize: 3,
        flavor: "hazelnut"
    },
    {
        name: "CP101",
        productType: "largeCoffeePod",
        packSize: 1,
        flavor: "vanilla"
    },
    {
        name: "CP103",
        productType: "largeCoffeePod",
        packSize: 3,
        flavor: "vanilla"
    },
    {
        name: "CP111",
        productType: "largeCoffeePod",
        packSize: 1,
        flavor: "caramel"
    },
    {
        name: "CP113",
        productType: "largeCoffeePod",
        packSize: 3,
        flavor: "caramel"
    },
    {
        name: "CP121",
        productType: "largeCoffeePod",
        packSize: 1,
        flavor: "psl"
    },
    {
        name: "CP123",
        productType: "largeCoffeePod",
        packSize: 3,
        flavor: "psl"
    },
    {
        name: "CP131",
        productType: "largeCoffeePod",
        packSize: 1,
        flavor: "mocha"
    },
    {
        name: "CP133",
        productType: "largeCoffeePod",
        packSize: 3,
        flavor: "mocha"
    },
    {
        name: "CP141",
        productType: "largeCoffeePod",
        packSize: 1,
        flavor: "hazelnut"
    },
    {
        name: "CP143",
        productType: "largeCoffeePod",
        packSize: 3,
        flavor: "hazelnut"
    },
    {
        name: "EP003",
        productType: "espressoPod",
        packSize: 3,
        flavor: "vanilla"
    },
    {
        name: "EP005",
        productType: "espressoPod",
        packSize: 5,
        flavor: "vanilla"
    },
    {
        name: "EP007",
        productType: "espressoPod",
        packSize: 7,
        flavor: "vanilla"
    },
    {
        name: "EP013",
        productType: "espressoPod",
        packSize: 3,
        flavor: "caramel"
    },
    {
        name: "EP015",
        productType: "espressoPod",
        packSize: 5,
        flavor: "caramel"
    },
    {
        name: "EP017",
        productType: "espressoPod",
        packSize: 7,
        flavor: "caramel"
    },
]