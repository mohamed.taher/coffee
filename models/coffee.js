const mongoose = require('mongoose');
const dataConfig = require("../dataConfig")

const coffeeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 1
    },
    productType: {
        type: String,
        minlength: 1
    },
    flavor: {
        type: String,
        minlength: 1
    },
    model: {
        type: String,
        minlength: 1
    },
    packSize: {
        type: Number,
        min: 1
    },
    waterLineCompatible: Boolean,
});


const Coffee = mongoose.model('Coffee', coffeeSchema);



let seedCoffee = async () => {
    let checkEmptyModel = await Coffee.find()

    if(checkEmptyModel.length) return

    let dataConfigArrStored = await Coffee.insertMany(dataConfig);

    return dataConfigArrStored
}

seedCoffee();

exports.Coffee = Coffee;