const Joi = require('joi');
const _ = require('lodash');
const { Coffee } = require("../models/coffee")
const { isNumber, strToBool } = require("../helper")


//validate coffee pods api
function validateCoffeePods(body) {
  const schema = {
    packSize: Joi.number().min(1).optional(),
    flavor: Joi.string().min(1).optional(),
    productType: Joi.string().min(1).optional(),
  };

  return Joi.validate(body, schema);
}

//validate cofe machines api
function validateCoffeeMachines(body) {
  const schema = {
    waterLineCompatible: Joi.boolean().optional(),
    productType: Joi.string().min(1).optional(),
  };

  return Joi.validate(body, schema);
}


//controller list coffee pods/machines according to type
exports.listCoffeeController = (type) => async (req, res) => {

  //get query strings for filtering
  let query = req.query;

  //special validation for packageSize
  if (query.packSize) {
    query.packSize = (isNumber(query.packSize)) ?
      parseInt(query.packSize) : 0
  }

  //special validation for waterLineCompatible
  if (query.waterLineCompatible) {
    query.waterLineCompatible =
      strToBool(query.packSize)
  }


  let validateFn = type == "machines" ?
    validateCoffeeMachines : validateCoffeePods;


  //validate query strings
  let { error } = validateFn(query);
  if (error) return res.status(401).send('parameters error');

  //check if contact actually exist
  let coffeeNames = await Coffee.find(query)

  return coffeeNames;

}

